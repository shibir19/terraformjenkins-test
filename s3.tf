resource "aws_s3_bucket" "a" {
  bucket = "shibirjenkins"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
